package com.smartphone.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.smartphone.entities.Role;


@Stateless
public class RoleDAO {
	
	private final static String UNIT_NAME = "SmartphoneService";
	
	
	//Wstrzykiwanie zaleznosci
	
	@PersistenceContext(unitName = UNIT_NAME)
	protected EntityManager em;
	
	public void create(Role role)
	{
		em.persist(role);
	}
	
	public Role merge (Role role)
	{
		return em.merge(role);
	}
	
	public void remove (Role role)
	{
		em.remove(em.merge(role));
	}
	
	public Role find(Object id)
	{
		return em.find(Role.class, id);
	}
	
	public List<Role> getAllRoles()
	{
		List <Role> list = null;
		Query query = em.createQuery("select p from Role p");
		try {
			list= query.getResultList();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return list;
	}
	
	public Role findRole(String rolename)
	{
		Role role = null;
		String select = "select p ";
		String from ="from Role p ";
		String where="where p.roleName = :rolename";
		System.out.println(select+from+where);
		Query query = em.createQuery(select+from+where);
		query.setParameter("rolename", rolename);
		try {
		role=(Role) query.getSingleResult();
		}
		catch(Exception e)
		{
			role=null;
			System.out.println("nie znalezlismy roli");
		}
		return role;
		
	}
	

}
