package com.smartphone.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.smartphone.entities.Service;

@Stateless
public class ServiceDAO {
private final static String UNIT_NAME = "SmartphoneService";

@PersistenceContext(unitName = UNIT_NAME)
protected EntityManager em;


public void create (Service service)
{
	em.persist(service);	
}

public Service merge (Service service)
{
	return em.merge(service);
}

 public void remove(Service service)
 {
	 em.remove(service);
 }
 
 public List<Service> getAllServices()
 {
	List<Service> services = null;
	String select="select p ";
	String from = "from Service p";
	
	Query query = em.createQuery(select + from);
	
	try {
		services= query.getResultList();
	}
	catch (Exception e)
	{
		services=null;
		System.out.println("Pusto");
	}
	System.out.println(services.size());
	return services;
 }
 
 public List<Service> getAllActiveServices()
 {
	List<Service> services = null;
	String select="select p ";
	String from = "from Service p ";
	String where=" where p.active = 1";
	
	Query query = em.createQuery(select + from + where);
	
	try {
		services= query.getResultList();
	}
	catch (Exception e)
	{
		services=null;
		System.out.println("Pusto");
	}
	System.out.println(services.size());
	return services;
 }
 

}
