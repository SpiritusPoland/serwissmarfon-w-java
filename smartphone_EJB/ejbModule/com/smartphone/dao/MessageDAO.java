package com.smartphone.dao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.smartphone.entities.Message;

@Stateless
public class MessageDAO {
	private final static String UNIT_NAME = "SmartphoneService";

	@PersistenceContext(unitName = UNIT_NAME)
	protected EntityManager em;
	
	public void create(Message message)
	{
		em.persist(message);
	}
	
	public Message merge(Message message)
	{
		return em.merge(message);
	}
	public void remove(Message message)
	{
		em.remove(em.merge(message));
	}
	
	
	
}
