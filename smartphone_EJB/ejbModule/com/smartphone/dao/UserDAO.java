package com.smartphone.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.smartphone.entities.Role;
import com.smartphone.entities.User;

@Stateless
public class UserDAO {
private final static String UNIT_NAME = "SmartphoneService";
	
@PersistenceContext(unitName = UNIT_NAME)
protected EntityManager em;

public void create(User user) {
	em.persist(user);
}

public User merge(User user)
{
	return em.merge(user);
}

public void remove (User user)
{
	em.remove(em.merge(user));
}


public User getUser(String login,String password)
{
	User user=null;
	
	String select = "select p ";
	String from = "from User p ";
	String where = "where p.login = :login AND p.password = :password";
	
	Query query = em.createQuery(select+from+where);
	query.setParameter("login", login);
	query.setParameter("password", password);
	
	try {
		user=(User) query.getSingleResult();
	} catch (Exception e)
	{
		System.out.println("Wyj�tek");
		e.printStackTrace();
	}
	return user;
}


public List<User> getAllUsers()
{
	List<User> users=null;
	
	String select = "select p ";
	String from = "from User p ";
	
	Query query = em.createQuery(select+from);
	
	
	try {
		users= query.getResultList();
	} catch (Exception e)
	{
		System.out.println("Wyj�tek");
		e.printStackTrace();
	}
	return users;
}

public boolean isEmailUnique(String email)
{
	User user=null;
	
	String select="select p ";
	String from = "from User p ";
	String where = "where p.email = :email";
	
	Query query = em.createQuery(select+from+where);
	query.setParameter("email", email);
	
	try {
		user = (User) query.getSingleResult();
	} catch (Exception e)
	{
		user=null;
	}
	
	if(user==null)
	{
		return true;
	}
	else 
		return false;
	
}

public boolean isLoginUnique(String login)
{
	User user=null;
	
	String select="select p ";
	String from = "from User p ";
	String where = "where p.login = :login";
	
	Query query = em.createQuery(select+from+where);
	query.setParameter("login", login);
	
	try {
		user = (User) query.getSingleResult();
	} catch (Exception e)
	{
		user=null;
	}
	
	if(user==null)
	{
		return true;
	}
	else 
		return false;
	
}

	
	
	
	
	
	
	
}
