package com.smartphone.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.smartphone.entities.Application;
import com.smartphone.entities.User;

@Stateless
public class ApplicationDAO {

	
	private final static String UNIT_NAME = "SmartphoneService";
	
	
	//Wstrzykiwanie zaleznosci
	
	@PersistenceContext(unitName = UNIT_NAME)
	protected EntityManager em;
	
	public void create(Application app)
	{
		em.persist(app);
	}
	
	public Application merge(Application app)
	{
		return em.merge(app);
	}
	
	public void remove(Application app)
	{
		em.remove(app);
	}
	
	
	public List<Application> getAllApplications()
	{
		List<Application> apps=null;
		
		String select = "select p ";
		String from = "from Application p ";
		
		Query query = em.createQuery(select+from);
		
		
		try {
			apps= query.getResultList();
		} catch (Exception e)
		{
			System.out.println("Wyj�tek");
			e.printStackTrace();
		}
		return apps;
	}
	
	public List<Application> getAllUserApplications(User user)
	{
		List<Application> apps=null;
		
		String select = "select p ";
		String from = "from Application p ";
		String where = "where p.user = :user";
		
		
		Query query = em.createQuery(select+from+ where);
		query.setParameter("user", user);
			
		
		try {
			apps= query.getResultList();
		} catch (Exception e)
		{
			System.out.println("Wyj�tek");
			e.printStackTrace();
		}
		return apps;
	}
	
}
