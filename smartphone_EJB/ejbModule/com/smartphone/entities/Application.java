package com.smartphone.entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the application database table.
 * 
 */
@Entity
@NamedQuery(name="Application.findAll", query="SELECT a FROM Application a")
public class Application implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="application_id")
	private int applicationId;

	private byte closed;

	@Lob
	private String description;

	@Lob
	private String topic;
	
	
	private Timestamp applicationCreationDate;

	//bi-directional many-to-one association to Service
	@ManyToOne
	@JoinColumn(name="service_id")
	private Service service;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="user_id")
	private User user;

	//bi-directional many-to-one association to Message
	@OneToMany(mappedBy="application")
	private List<Message> messages;

	public Application() {
	}

	public Timestamp getApplicationCreationDate() {
		return this.applicationCreationDate;
	}

	public void setApplicationCreationDate(Timestamp applicationCreationDate) {
		this.applicationCreationDate = applicationCreationDate;
	}
	
	public int getApplicationId() {
		return this.applicationId;
	}

	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}

	public byte getClosed() {
		return this.closed;
	}

	public void setClosed(byte closed) {
		this.closed = closed;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTopic() {
		return this.topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public Service getService() {
		return this.service;
	}

	public void setService(Service service) {
		this.service = service;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Message> getMessages() {
		return this.messages;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}

	public Message addMessage(Message message) {
		getMessages().add(message);
		message.setApplication(this);

		return message;
	}

	public Message removeMessage(Message message) {
		getMessages().remove(message);
		message.setApplication(null);

		return message;
	}
	
	public boolean isClosed()
	{
		if(this.closed==0)
		{
			return false;
		}
		else
			return true;
	}
	
	public boolean isIdTheSame(Application app)
	{
		if(app==null)
			return false;
		if(this.applicationId==app.applicationId)
		{
			System.out.println("takie same");
			return true;
		}
		else
			System.out.println("r�ne");

			return false;
	}
	
	public String shortDesc()
	{
		int length = this.description.length();
		int endindex=length;
		if(length>=400)
			endindex=400;
		return this.description.substring(0, endindex)+"...";
	}

}