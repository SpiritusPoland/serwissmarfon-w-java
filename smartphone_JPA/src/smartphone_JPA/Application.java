package smartphone_JPA;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the application database table.
 * 
 */
@Entity
@NamedQuery(name="Application.findAll", query="SELECT a FROM Application a")
public class Application implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="application_id")
	private int applicationId;

	private Timestamp applicationCreationDate;

	private byte closed;

	@Lob
	private String description;

	@Column(name="service_id")
	private int serviceId;

	@Lob
	private String topic;

	@Column(name="user_id")
	private int userId;

	public Application() {
	}

	public int getApplicationId() {
		return this.applicationId;
	}

	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}

	public Timestamp getApplicationCreationDate() {
		return this.applicationCreationDate;
	}

	public void setApplicationCreationDate(Timestamp applicationCreationDate) {
		this.applicationCreationDate = applicationCreationDate;
	}

	public byte getClosed() {
		return this.closed;
	}

	public void setClosed(byte closed) {
		this.closed = closed;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getServiceId() {
		return this.serviceId;
	}

	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}

	public String getTopic() {
		return this.topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}