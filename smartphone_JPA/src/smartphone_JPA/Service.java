package smartphone_JPA;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the services database table.
 * 
 */
@Entity
@Table(name="services")
@NamedQuery(name="Service.findAll", query="SELECT s FROM Service s")
public class Service implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="service_id")
	private int serviceId;

	private byte active;

	@Column(name="service_cost")
	private String serviceCost;

	@Column(name="service_name")
	private String serviceName;

	private String servicescol;

	//bi-directional many-to-one association to Application
	@OneToMany(mappedBy="service")
	private List<Application> applications;

	public Service() {
	}

	public int getServiceId() {
		return this.serviceId;
	}

	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}

	public byte getActive() {
		return this.active;
	}

	public void setActive(byte active) {
		this.active = active;
	}

	public String getServiceCost() {
		return this.serviceCost;
	}

	public void setServiceCost(String serviceCost) {
		this.serviceCost = serviceCost;
	}

	public String getServiceName() {
		return this.serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getServicescol() {
		return this.servicescol;
	}

	public void setServicescol(String servicescol) {
		this.servicescol = servicescol;
	}

	public List<Application> getApplications() {
		return this.applications;
	}

	public void setApplications(List<Application> applications) {
		this.applications = applications;
	}

	public Application addApplication(Application application) {
		getApplications().add(application);
		application.setService(this);

		return application;
	}

	public Application removeApplication(Application application) {
		getApplications().remove(application);
		application.setService(null);

		return application;
	}

}