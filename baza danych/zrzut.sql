-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: smartphone-service
-- ------------------------------------------------------
-- Server version	8.0.14

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `application`
--

DROP TABLE IF EXISTS `application`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `application` (
  `application_id` int(11) NOT NULL AUTO_INCREMENT,
  `topic` text COLLATE utf8_polish_ci NOT NULL,
  `description` text COLLATE utf8_polish_ci NOT NULL,
  `closed` tinyint(4) NOT NULL,
  `service_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `applicationCreationDate` timestamp NOT NULL,
  PRIMARY KEY (`application_id`),
  KEY `fk_application_services1_idx` (`service_id`),
  KEY `fk_application_users1_idx` (`user_id`),
  CONSTRAINT `fk_application_services1` FOREIGN KEY (`service_id`) REFERENCES `services` (`service_id`),
  CONSTRAINT `fk_application_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `application`
--

LOCK TABLES `application` WRITE;
/*!40000 ALTER TABLE `application` DISABLE KEYS */;
INSERT INTO `application` VALUES (1,'ghjkgy','ftuyjhghj',1,1,1,'2019-05-06 21:26:37'),(2,'ghjkgy','ftuyjhghj',0,1,5,'2019-05-06 21:33:22'),(3,'ghjkgy','ftuyjhghj',0,1,1,'2019-05-06 21:33:23'),(4,'dfgfdg','dfgdfg',0,1,2,'2019-05-06 21:53:25'),(5,'dfgfdg','dfgdfg',0,1,1,'2019-05-06 21:53:27'),(6,'dfgfdg','dfgdfg',0,1,1,'2019-05-06 21:53:28'),(7,'dfgfdg','dfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifddfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdgasdasdasdhidhffgghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifddfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdgasdasdasdhidhffgghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifddfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdgasdasdasdhidhffgghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifddfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdgasdasdasdhidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifddfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdgasdasdasdhidhfdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifddfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdgasdasdasdhidhffggdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifddfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdgasdasdasdhidhffggdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifddfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdgasdasdasdhidhffggdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifddfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdgasdasdasdhidhffggdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifddfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdgasdasdasdhidhffgghidhdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifddfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdgasdasdasdhidhffgghidhdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifddfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdgasdasdasdhidhffggdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifddfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdgasdasdasdhidhffggdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifddfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdgasdasdasdhidhffggdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifddfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdgasdasdasdhidhffggvhidhffghidhffghidhffghidhffgffgffghidhffghidhffghidhffghidhffgfgghidhffgghidhffg',0,1,1,'2019-05-06 21:53:28'),(8,'dfgfdg','dfgdfg',0,1,2,'2019-05-06 21:55:58'),(9,'dfgfdg','dfgdfgasdas',0,1,1,'2019-05-06 21:56:01'),(10,'dfgfdg','dfgdfg',0,1,1,'2019-05-06 21:59:43'),(11,'dfgfdg','dfgdfg',0,1,5,'2019-05-06 21:59:44'),(12,'dfgfdg','dfgdfg',0,1,5,'2019-05-06 21:59:57'),(13,'dfgfdg','dfgdfg',0,1,1,'2019-05-06 22:00:42');
/*!40000 ALTER TABLE `application` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `messages` (
  `message_id` int(11) NOT NULL,
  `message_timestamp` timestamp NULL DEFAULT NULL,
  `content` text COLLATE utf8_polish_ci,
  `users_user_id` int(11) NOT NULL,
  `application_application_id` int(11) NOT NULL,
  PRIMARY KEY (`message_id`),
  KEY `fk_messages_users1_idx` (`users_user_id`),
  KEY `fk_messages_application1_idx` (`application_application_id`),
  CONSTRAINT `fk_messages_application1` FOREIGN KEY (`application_application_id`) REFERENCES `application` (`application_id`),
  CONSTRAINT `fk_messages_users1` FOREIGN KEY (`users_user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(45) COLLATE utf8_polish_ci DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'admin'),(2,'user');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `services` (
  `service_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_name` varchar(45) COLLATE utf8_polish_ci DEFAULT NULL,
  `service_cost` varchar(45) COLLATE utf8_polish_ci DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `servicescol` varchar(45) COLLATE utf8_polish_ci DEFAULT NULL,
  PRIMARY KEY (`service_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` VALUES (1,'wymiana wyswietlacza','100zł',1,NULL),(2,'wymiana wejscia jack','45zł',1,NULL),(3,'konfiguracja telefonu','20zł',1,NULL);
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(45) COLLATE utf8_polish_ci NOT NULL,
  `password` varchar(90) COLLATE utf8_polish_ci NOT NULL,
  `email` varchar(45) COLLATE utf8_polish_ci NOT NULL,
  `role` int(11) NOT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deletion_timestamp` timestamp NULL DEFAULT NULL,
  `register_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `fk_users_role_idx` (`role`),
  CONSTRAINT `fk_users_role` FOREIGN KEY (`role`) REFERENCES `role` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'malina','malina1','malina@oasfgui.pl',2,0,NULL,'2019-04-30 13:07:14'),(2,'admin','admin','user@malina.pl',1,0,NULL,'2019-05-05 13:17:24'),(5,'karol','karol','karol@das.pas',1,0,NULL,NULL),(6,'admin2','admin','admin@asdasd.pl',2,0,NULL,NULL),(7,'lolololasd','asddfg','sdffs@asdf.pl',1,0,NULL,NULL),(8,'231dffghg23','rfstghfgh','dfgdfgg@fghfg.pl',1,0,NULL,NULL),(9,'dfgvd','sdfdfsds','asdffsd@sdfg.pl',1,0,NULL,NULL),(10,'sdfdsf','ds33','12fsd@sad.pl',1,0,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-07  5:53:26
