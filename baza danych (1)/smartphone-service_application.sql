-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: smartphone-service
-- ------------------------------------------------------
-- Server version	8.0.14

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `application`
--

DROP TABLE IF EXISTS `application`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `application` (
  `application_id` int(11) NOT NULL AUTO_INCREMENT,
  `topic` text COLLATE utf8_polish_ci NOT NULL,
  `description` text COLLATE utf8_polish_ci NOT NULL,
  `closed` tinyint(4) NOT NULL,
  `service_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `applicationCreationDate` timestamp NOT NULL,
  PRIMARY KEY (`application_id`),
  KEY `fk_application_services1_idx` (`service_id`),
  KEY `fk_application_users1_idx` (`user_id`),
  CONSTRAINT `fk_application_services1` FOREIGN KEY (`service_id`) REFERENCES `services` (`service_id`),
  CONSTRAINT `fk_application_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `application`
--

LOCK TABLES `application` WRITE;
/*!40000 ALTER TABLE `application` DISABLE KEYS */;
INSERT INTO `application` VALUES (1,'ghjkgy','ftuyjhghj',1,1,1,'2019-05-06 21:26:37'),(2,'ghjkgy','ftuyjhghj',0,1,5,'2019-05-06 21:33:22'),(3,'ghjkgy','ftuyjhghj',0,1,1,'2019-05-06 21:33:23'),(4,'dfgfdg','dfgdfg',0,1,2,'2019-05-06 21:53:25'),(5,'dfgfdg','dfgdfg',0,1,1,'2019-05-06 21:53:27'),(6,'dfgfdg','dfgdfg',0,1,1,'2019-05-06 21:53:28'),(7,'dfgfdg','dfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifddfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdgasdasdasdhidhffgghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifddfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdgasdasdasdhidhffgghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifddfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdgasdasdasdhidhffgghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifddfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdgasdasdasdhidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifddfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdgasdasdasdhidhfdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifddfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdgasdasdasdhidhffggdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifddfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdgasdasdasdhidhffggdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifddfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdgasdasdasdhidhffggdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifddfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdgasdasdasdhidhffggdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifddfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdgasdasdasdhidhffgghidhdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifddfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdgasdasdasdhidhffgghidhdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifddfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdgasdasdasdhidhffggdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifddfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdgasdasdasdhidhffggdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifddfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdgasdasdasdhidhffggdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifddfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdghidhffgdfgdsdgfsijgifdgasdasdasdhidhffggvhidhffghidhffghidhffghidhffgffgffghidhffghidhffghidhffghidhffgfgghidhffgghidhffg',0,1,1,'2019-05-06 21:53:28'),(8,'dfgfdg','dfgdfg',0,1,2,'2019-05-06 21:55:58'),(9,'dfgfdg','dfgdfgasdas',0,1,1,'2019-05-06 21:56:01'),(10,'dfgfdg','dfgdfg',0,1,1,'2019-05-06 21:59:43'),(11,'dfgfdg','dfgdfg',0,1,5,'2019-05-06 21:59:44'),(12,'dfgfdg','dfgdfg',0,1,5,'2019-05-06 21:59:57'),(13,'dfgfdg','dfgdfg',0,1,1,'2019-05-06 22:00:42');
/*!40000 ALTER TABLE `application` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-07  5:52:33
