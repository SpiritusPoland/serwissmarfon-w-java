package com.jsfcourse.login;

import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.simplesecurity.RemoteClient;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.smartphone.dao.UserDAO;
import com.smartphone.entities.Role;
import com.smartphone.entities.User;

@Named
@RequestScoped
public class LoginBB {
	private static final String PAGE_LOGIN = "/public/index?faces-redirect=true";
	private static final String PAGE_STAY_AT_THE_SAME = null;

	private String login;
	private String pass;
	
	private ResourceBundle messages;
	
	
	@Inject
	@EJB
	UserDAO userDAO;
	
	@PostConstruct
    public void initialization() {
		FacesContext context = FacesContext.getCurrentInstance();
		this.messages = ResourceBundle.getBundle("resources.login", context.getViewRoot().getLocale());
	}
	
	public String loginIn() {
		FacesContext ctx = FacesContext.getCurrentInstance();
		// 1. verify login and password - get User from "database"
		User user = userDAO.getUser(login, pass);
		
		// 2. if bad login or password - stay with error info
		if (user == null) {
			ctx.addMessage("login", new FacesMessage(FacesMessage.SEVERITY_ERROR,
					messages.getString("incorrectLoginOrPass"), null));
			return PAGE_STAY_AT_THE_SAME;
		} else if (user.isDeleted())
		{
			ctx.addMessage("login", new FacesMessage(FacesMessage.SEVERITY_ERROR,
					messages.getString("deletedAccount"), null));
			return PAGE_STAY_AT_THE_SAME;
		}

		// 3. jezeli zalogowany pobierz role i zapisz je w RemoteClient i przechowaj w sesji
		//if logged in: get User roles, save in RemoteClient and store it in session
		
		RemoteClient<User> client = new RemoteClient<User>(); //create new RemoteClient
		client.setDetails(user);
		
		Role role=user.getRoleBean();//pobranie roli;
		client.getRoles().add(role.getRoleName());

	
		//store RemoteClient with request info in session (needed for SecurityFilter)
		HttpServletRequest request = (HttpServletRequest) ctx.getExternalContext().getRequest();
		client.store(request);
		// and enter the system (now SecurityFilter will pass the request)
		if(client.isInRole("admin"))
		{
			return PAGE_LOGIN;
		}
		else
		{
			return PAGE_LOGIN;
		}
	}
	
	public static String doLogout(){
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance()
				.getExternalContext().getSession(true);
		//Invalidate session
		// - all objects within session will be destroyed
		// - new session will be created (with new ID)
		session.invalidate();
		return PAGE_LOGIN;
	}

	
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public ResourceBundle getMessages() {
		return messages;
	}

	public void setMessages(ResourceBundle messages) {
		this.messages = messages;
	}
	

	
}
