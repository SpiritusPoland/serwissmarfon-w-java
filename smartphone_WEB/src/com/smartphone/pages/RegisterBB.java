package com.smartphone.pages;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.sql.Timestamp;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import com.smartphone.dao.RoleDAO;
import com.smartphone.dao.UserDAO;
import com.smartphone.entities.Role;
import com.smartphone.entities.User;

@Named
@ViewScoped
public class RegisterBB implements Serializable{

	
	//?
	private static final long serialVersionUID = 1L;

	private static final String PAGE_STAY_AT_THE_SAME = null;
	private static final String REGISTER_SUCCESFULL="/public/registrationSucces";
	
	private User user = new User();
	private String passConfirm;
	
	
	private User loaded = null;
	
	@EJB
	UserDAO userDAO;
	@EJB
	RoleDAO roleDAO;
	
	private ResourceBundle messages;
	
	public ResourceBundle getMessages()
	{
		return messages;
	}
	
	public void setMessages(ResourceBundle messages)
	{
		this.messages=messages;
	}
	
	@PostConstruct
	public void PostConstruct()
	{
		//pobranie odpowiedniej paczki z resources
		FacesContext context = FacesContext.getCurrentInstance();
		this.messages= ResourceBundle.getBundle("resources.regbean",context.getViewRoot().getLocale());
	}
	
	public void onLoad() throws IOException 
	{
		FacesContext context = FacesContext.getCurrentInstance();
	
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		
		loaded = (User) session.getAttribute("user");
		
		//cleaning
		if(loaded != null)
		{
			user=loaded;
			session.removeAttribute("user");
		} else
		{
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, messages.getString("systemError"), null));
		}		
	}
	
	
	//zapisz dane
	public String saveData() {
		//if email exists in database:
		if(!userDAO.isEmailUnique(user.getEmail()) && user.getUserId() == 0)
		{
			FacesContext.getCurrentInstance().addMessage("email", new FacesMessage(FacesMessage.SEVERITY_WARN, messages.getString("uniqueEmail"), null));
			return PAGE_STAY_AT_THE_SAME;
		}
		if(!userDAO.isLoginUnique(user.getLogin()) && user.getUserId() == 0)
		{
			FacesContext.getCurrentInstance().addMessage("login", new FacesMessage(FacesMessage.SEVERITY_WARN, messages.getString("uniqueLogin"), null));
			return PAGE_STAY_AT_THE_SAME;
		}
		
		
		//if pass and passConfirm are different
		if(!(user.getPassword().equals(passConfirm)))
		{
			
			FacesContext.getCurrentInstance().addMessage("password", new FacesMessage(FacesMessage.SEVERITY_WARN, messages.getString("passMismatch"), null));
			return PAGE_STAY_AT_THE_SAME;
		}			
		else
		{
			
			try {
				if (user.getUserId() == 0)
				{
					
					Role role;
					role=roleDAO.findRole("user");
					user.setRoleBean(role);
					
					Date date= new Date();
					Timestamp time=new Timestamp(date.getTime());
					
					user.setRegisterDate(time);

					userDAO.create(user);
				} else {
					// existing record
					userDAO.merge(user);
				}
			} catch (Exception e) {
				e.printStackTrace();
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, messages.getString("saveError"), null));
				return PAGE_STAY_AT_THE_SAME;
			}
			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("registerSuccessful", messages.getString("registerSuccess"));
		    return REGISTER_SUCCESFULL;
		}
	}
	public void print(String a)
	{
		System.out.println(a);
	}
	
	/*GETERS SETERS*/
	
	public User getUser()
	{
		return user;
	}
	public void setPassConfirm(String passConfirm)
	{
		this.passConfirm=passConfirm;	
	}
	public String getPassConfirm()
	{
		return passConfirm;
	}
	
	
}
