package com.smartphone.user;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.faces.simplesecurity.RemoteClient;
import javax.faces.view.ViewScoped;
import javax.servlet.http.HttpSession;

import com.jsfcourse.login.LoginBB;
import com.smartphone.dao.ApplicationDAO;
import com.smartphone.dao.MessageDAO;
import com.smartphone.dao.ServiceDAO;
import com.smartphone.dao.UserDAO;
import com.smartphone.entities.Application;
import com.smartphone.entities.Message;
import com.smartphone.entities.Service;
import com.smartphone.entities.User;


@ManagedBean
@ViewScoped
public class UserBB implements Serializable {
	private static final String PAGE_MYPROFILE = "myprofile?faces-redirect=true";
	private static final String PAGE_MYAPPLICATIONS ="/pages/user/myApplications?faces-redirect=true";
	private static final String PAGE_ADDAPPLICATION="/pages/user/addApplication?faces-redirect=true";
	private static final String STAY_AT_THE_SAME = null;
	
	/**
	 * 
	 */	private static final long serialVersionUID = 1L;
	
	
	@EJB
	UserDAO userDAO;
	
	@EJB
	ApplicationDAO appDAO;	
	
	@EJB
	ServiceDAO serviceDAO;
	
	@EJB
	MessageDAO messageDAO;
	
	private List<Service> services= null;
	private ResourceBundle messages;
	private User user;
	private String pass=null;
	private String passConfirm=null;
	private String email;
	private Application application= null;
	private Application modalApp= null;
	

	public Application getModalApp() {
		return modalApp;
	}

	public void setModalApp(Application modalApp) {
		System.out.println("ustawiam");
		this.modalApp = modalApp;
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
		session.setAttribute("modal", modalApp);
	}

	private String messContent= null;
	
	public String getMessContent() {
		return messContent;
	}

	public void setMessContent(String messContent) {
		this.messContent = messContent;
	}

	private Service service=null;
	private String applicationDescription=null;
	private String applicationTopic=null;
	private List<Application> apps=null;
	
	
	
	@PostConstruct
	public void initialization()
	{
		
		services = serviceDAO.getAllActiveServices();
		FacesContext context = FacesContext.getCurrentInstance();
		this.messages= ResourceBundle.getBundle("resources.regbean",context.getViewRoot().getLocale());
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
		RemoteClient<User> client =(RemoteClient<User>) RemoteClient.load(session);
		user=client.getDetails();	
		email=new String(user.getEmail());
		setApps(appDAO.getAllUserApplications(user));
		try {
		modalApp=(Application)session.getAttribute("modal");
		}
		catch (Exception e)
		{
			modalApp=null;
		}
		
	}
	
	public void show()
	{
		System.out.println(modalApp.getTopic());
	}
	public void setApp(Application app)
	{
		this.application=app;
	}
	public void closeApplication(Application app)
	{
	app.setClosed((byte)1);
	appDAO.merge(app);
		
	}
	
	public String myProfile()
	{
		return PAGE_MYPROFILE;
	}	
	
	public String myApplications()
	{
		return PAGE_MYAPPLICATIONS;
	}
	
	public String addApplication()
	{
		return PAGE_ADDAPPLICATION;
	}

	
	public void saveChanges()
	{
		 int change=0;
		if(pass!=null && passConfirm!=null && !pass.equals("") && !passConfirm.equals(""))
		{
			if(pass.length()<6 || pass.length()>40)
			{
				FacesContext.getCurrentInstance().addMessage("password", new FacesMessage(FacesMessage.SEVERITY_WARN, messages.getString("passLength"), null));
			}
			if(passConfirm.length()<6 || passConfirm.length()>40)
			{
				FacesContext.getCurrentInstance().addMessage("passConfirm", new FacesMessage(FacesMessage.SEVERITY_WARN, messages.getString("passLength"), null));
			}
			if(pass.length()>=6 && pass.length()<=40 && passConfirm.length()>=6 && passConfirm.length()<=40)
			{
			if(!pass.equals(passConfirm))//r�ne has�a
			{
				FacesContext.getCurrentInstance().addMessage("password", new FacesMessage(FacesMessage.SEVERITY_WARN, messages.getString("passMismatch"), null));
			}
			else
			{
				if(!user.getPassword().equals(this.pass))
				{
					user.setPassword(this.pass);
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, messages.getString("passChanged"), null));
					change++;
				}
			}
			}
			
		
		}
		if(!this.email.equals(user.getEmail()))//zmieniony email
		{
			if(!userDAO.isEmailUnique(this.email))//czy email jest unikatowy?
			{
				FacesContext.getCurrentInstance().addMessage("email", new FacesMessage(FacesMessage.SEVERITY_WARN, messages.getString("uniqueEmail"), null));
			}
			else
			{
				user.setEmail(this.email);
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, messages.getString("emailChanged"), null));
				change++;
			}
		}
		if(change>0)
		{
			userDAO.merge(this.user);
		}
		
		
	}
	
	public String deleteAccount()
	{
		user.setDeletion(true);
		Date date= new Date();
		Timestamp time=new Timestamp(date.getTime());		
		user.setDeletionTimestamp(time);
		userDAO.merge(user);
		return LoginBB.doLogout();
	}
	
	public String sendApplication()
	{
		
		Date date= new Date();		
		Timestamp time=new Timestamp(date.getTime());
		application=new Application();
		application.setDescription(this.applicationDescription);
		application.setTopic(this.applicationTopic);
		application.setService(this.service);
		application.setUser(this.user);
		application.setClosed((byte) 0);		
		application.setApplicationCreationDate(time);		
		appDAO.create(application);
				
		return null;
		
		
	}
	/* GETERS SETERS*/
	public List<Service> getServices()
	{
		return services;
	}
	
	public void setServices(List<Service> services)
	{
		this.services=services;
	}
	
	public User getUser()
	{
		return user;
	}

	public String getEmail()
	{
		return email;
	}
	
	public void setEmail(String email)
	{
		this.email=email;
	}

	public String getPass()
	{
		return this.pass;
	}
	
	public void setPass(String password)
	{
		this.pass=password;
	}
	
	public String getPassConfirm()
	{
		return this.passConfirm;
	}
	
	public void setPassConfirm(String passwordConfirm)
	{
		this.passConfirm=passwordConfirm;
	}
	
	public ResourceBundle getMessages()
	{
		return messages;
	}
	
	public String getCos()
	{
		return messContent;
	}
	
	public void setMessages(ResourceBundle messages)
	{
		this.messages=messages;
	}

	public Service getService()
	{
		return service;
	}
	
	public void setService(Service service)
	{
		this.service=service;
	}
	
	public String getApplicationDescription() {
		return applicationDescription;
	}

	public void setApplicationDescription(String applicationDescription) {
		this.applicationDescription = applicationDescription;
	}

	public String getApplicationTopic() {
		return applicationTopic;
	}

	public void setApplicationTopic(String applicationTopic) {
		this.applicationTopic = applicationTopic;
	}

	public List<Application> getApps() {
		return apps;
	}

	public void setApps(List<Application> apps) {
		this.apps = apps;
	}
	
	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}


}
