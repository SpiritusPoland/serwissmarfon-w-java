package com.smartphone.admin;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;

import com.jsfcourse.login.LoginBB;
import com.smartphone.dao.ApplicationDAO;
import com.smartphone.dao.RoleDAO;
import com.smartphone.dao.UserDAO;
import com.smartphone.entities.Application;
import com.smartphone.entities.User;

@ManagedBean
@ViewScoped
public class AdminBB implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final String STAY_AT_THE_SAME = null;
	
	@EJB
	UserDAO userDAO;
	
	@EJB
	RoleDAO roleDAO;
	
	@EJB
	ApplicationDAO appDAO;
	
	List<User> allUsers = null;
	List <Application> allApps = null;

	
	


	@PostConstruct
	public void initialize()
	{
		allUsers=userDAO.getAllUsers();
		allApps=appDAO.getAllApplications();
	}


	public List<Application> getAllApps() {
		return allApps;
	}


	public void setAllApps(List<Application> allApps) {
		this.allApps = allApps;
	}


	public void giveAdmin(User user)
	{
		user.setRoleBean(roleDAO.findRole("admin"));
		userDAO.merge(user);
	}
	
	
	public void takeAdmin(User user)
	{
		user.setRoleBean(roleDAO.findRole("user"));
		userDAO.merge(user);
	}
	
	public void deleteUser(User user)
	{
		user.setDeletion(true);
		Date date= new Date();
		Timestamp time=new Timestamp(date.getTime());		
		user.setDeletionTimestamp(time);
		userDAO.merge(user);
	}
	
	public void restoreUser(User user)
	{
		user.setDeletion(false);
		user.setDeletionTimestamp(null);
		userDAO.merge(user);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public List<User> getAllUsers() {
		return allUsers;
	}



	public void setAllUsers(List<User> allUsers) {
		this.allUsers = allUsers;
	}

}
